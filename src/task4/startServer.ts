import express from "express";
import loaders from "./loaders";
import winston from "winston";
require("dotenv").config();

export default async function startServer() {
  const app = express();

  await loaders({expressApp: app});

  if (process.env.NODE_ENV !== "test") {
    app.listen(process.env.NODE_PORT, err => {
      if (err) {
        console.log(err);
        return;
      }
      console.log(`Your server is ready !`);
    });
  }

  process.on("uncaughtException", err =>
    winston.error("Uncaught exception: ", err)
  );

  return app;
}
