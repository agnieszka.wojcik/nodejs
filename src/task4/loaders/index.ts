import dbLoader from "./db";
import expressLoader from "./express";
import loggerLoader from "./logger";
import routesLoader from "./routes";

export default async function({expressApp}) {
  await dbLoader();
  await expressLoader({app: expressApp});
  await loggerLoader({app: expressApp});
  await routesLoader({app: expressApp});
}
