const cls = require("cls-hooked");
const Sequelize = require("sequelize");
require("dotenv").config();

const namespace = cls.createNamespace("nodejs-mentoring");
Sequelize.useCLS(namespace);

const sequelize = new Sequelize(process.env.DB_URL, {
  dialect: "postgres",
  protocol: "postgres",
  dialectOptions: {
    ssl: true
  }
});

export default async function() {
  try {
    await sequelize.authenticate();
    await sequelize.sync();
    console.log("Connection has been established successfully.");
  } catch (err) {
    console.error("Unable to connect to the database:", err);
  }
}
