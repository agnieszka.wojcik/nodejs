import express from "express";
import * as routes from "../routes";

export default async ({ app }: { app: express.Application }) => {  
  await routes.init({ app });
}
