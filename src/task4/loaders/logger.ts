import express from "express";
import winston from "winston";
import expressWinston from 'express-winston';

export default async ({ app }: { app: express.Application }) => { 
  app.use((req, res, next) => {
    console.log(`Request: ${req.method} ${req.url}`);
    next();
  });
  
  app.use(expressWinston.logger({
    transports: [
      new winston.transports.Console()
    ],
    format: winston.format.combine(
      winston.format.colorize(),
      winston.format.timestamp(),
      winston.format.align(),
      winston.format.printf((info) => {
        const {
          timestamp, level, message, ...args
        } = info;
        const ts = timestamp.slice(0, 19).replace('T', ' ');
        return `${ts} [${level}]: ${message} ${Object.keys(args).length ? JSON.stringify(args, null, 2) : ''}`;
      })
    ),
    meta: true,
    msg: "HTTP {{req.method}} {{req.url}}",
    expressFormat: false,
    colorize: true,
    ignoreRoute: function (req, res) { return false; },
    level: function (req, res) {
      var level = "";
      if (res.statusCode >= 100) { level = "info"; }
      if (res.statusCode >= 400) { level = "warn"; }
      if (res.statusCode >= 500) { level = "error"; }
      if (res.statusCode == 401 || res.statusCode == 403) { level = "critical"; }
      if (req.path === "/v1" && level === "info") { level = "error"; }
      return level;
    }
  }));
}