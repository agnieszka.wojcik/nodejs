import request from "supertest";
import uuid from "uuid/v1";
import startServer from "../../startServer";

import {GroupType} from "../../typings";

let app;
beforeEach(async () => {
  app = await startServer();
});

describe("Groups Endpoints", () => {
  describe("/GET groups", () => {
    it("should display a list of groups", done => {
      return request(app)
        .get("/groups")
        .set("Accept", "application/json")
        .expect("Content-Type", "application/json; charset=utf-8")
        .expect(200, done);
    });
  });

  describe("/GET group", () => {
    it("should display a group", done => {
      const id: string = "f991bc60-69bb-11ea-9e76-f70b873b6b8e";

      request(app)
        .get(`/group/${id}`)
        .set("Accept", "application/json")
        .expect("Content-Type", "application/json; charset=utf-8")
        .expect(res => {
          res.body = {
            id,
            name: "Superheroes",
            permissions: ["READ", "WRITE"],
            createdAt: "2020-03-19T08:31:05.511Z",
            updatedAt: "2020-03-19T08:31:05.511Z"
          };
        })
        .expect(200, done);
    });
  });

  describe("/POST group", () => {
    it("should create group", done => {
      const mockGroup: GroupType = {
        id: uuid(),
        name: "Children",
        permissions: ["READ", "WRITE"]
      };

      request(app)
        .post("/group")
        .send(mockGroup)
        .set("Accept", "application/json")
        .expect("Content-Type", "application/json; charset=utf-8")
        .expect(res => {
          res.body.name = "Children";
          res.body.permissions = ["READ", "WRITE"];
        })
        .expect(200, done);
    });
  });

  describe("/PUT group", () => {
    it("should update group", done => {
      const mockGroup = {
        name: "Pets"
      };
      const id: string = "43de1af0-69c3-11ea-83f5-c95f7fba3f35";

      request(app)
        .put(`/group/${id}`)
        .send(mockGroup)
        .set("Accept", "application/json")
        .expect("Content-Type", "application/json; charset=utf-8")
        .expect(res => {
          res.body.name = "Pets";
        })
        .expect(200, done);
    });
  });

  describe("/DELETE group", () => {
    it("should delete group", async () => {
      const mockGroup: GroupType = {
        id: uuid(),
        name: "Workers",
        permissions: ["READ", "WRITE"]
      };
      jest.setTimeout(30000);

      await request(app)
        .post("/group")
        .send(mockGroup)
        .set("Accept", "application/json");

      await request(app)
        .delete(`/group/${mockGroup.id}`)
        .set("Accept", "application/json")
        .expect("Content-Type", "application/json; charset=utf-8")
        .expect(200);
    });
  });
});
