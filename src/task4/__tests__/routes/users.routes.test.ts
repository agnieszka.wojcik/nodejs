import request from "supertest";
import uuid from "uuid/v1";
import startServer from "../../startServer";
import {UserType} from "../../typings";

let app;
beforeEach(async () => {
  app = await startServer();
});

describe("Users Endpoints", () => {
  describe("/GET users", () => {
    it("should display a list of users", done => {
      request(app)
        .get("/users")
        .set("Accept", "application/json")
        .expect("Content-Type", "application/json; charset=utf-8")
        .expect(200, done);
    });
  });

  describe("/GET user", () => {
    it("should display a user", done => {
      const id: string = "417222b0-4b74-11ea-a570-ff383585d999";

      request(app)
        .get(`/user/${id}`)
        .set("Accept", "application/json")
        .expect("Content-Type", "application/json; charset=utf-8")
        .expect(res => {
          res.body = {
            age: 40,
            createdAt: "2020-02-09T19:42:07.551Z",
            id,
            isDeleted: false,
            login: "joker",
            password: "g4g5g4g45fsfd",
            updatedAt: "2020-02-09T19:42:07.551Z"
          };
        })
        .expect(200, done);
    });
  });

  describe("/POST users", () => {
    it("should create user", done => {
      const mockUser: UserType = {
        id: uuid(),
        login: "flash",
        password: "gfg8df9gfd",
        age: 14,
        isDeleted: false
      };

      request(app)
        .post("/user")
        .send(mockUser)
        .set("Accept", "application/json")
        .expect("Content-Type", "application/json; charset=utf-8")
        .expect(res => {
          res.body.login = "flash";
          res.body.password = "gfg8df9gfd";
          res.body.age = 14;
          res.body.isDeleted = false;
        })
        .expect(200, done);
    });
  });

  describe("/PUT user", () => {
    it("should update user", done => {
      const mockUser: UserType = {
        login: "catwoman",
        password: "fgfdgfgfgsrtrytr",
        age: 10,
        isDeleted: false
      };
      const id = "41905910-4b74-11ea-a570-ff383585d999";
      request(app)
        .put(`/user/${id}`)
        .send(mockUser)
        .set("Accept", "application/json")
        .expect("Content-Type", "application/json; charset=utf-8")
        .expect(res => {
          res.body.login = "catwoman";
          res.body.password = "fgfdgfgfgsrtrytr";
          res.body.age = 10;
          res.body.isDeleted = false;
        })
        .expect(200, done);
    });
  });

  describe("/GET auto-suggest", () => {
    it("should display auto-suggest list", done => {
      const loginSubstring: string = "ker";

      request(app)
        .get(`/auto-suggest/${loginSubstring}`)
        .set("Accept", "application/json")
        .expect("Content-Type", "application/json; charset=utf-8")
        .expect(res => {
          res.body = {
            age: 40,
            createdAt: "2020-02-09T19:42:07.551Z",
            id: "417222b0-4b74-11ea-a570-ff383585d999",
            isDeleted: false,
            login: "joker",
            password: "g4g5g4g45fsfd",
            updatedAt: "2020-02-09T19:42:07.551Z"
          };
        })
        .expect(200, done);
    });
  });

  describe("/DELETE user", () => {
    it("should add flag isDeleted", done => {
      const id: string = "11";

      request(app)
        .delete(`/user/${id}`)
        .set("Accept", "application/json")
        .expect("Content-Type", "application/json; charset=utf-8")
        .expect(res => {
          res.body.login = "agadadsa";
          res.body.password = "nkkn55";
          res.body.age = 10;
          res.body.isDeleted = true;
        })
        .expect(200, done);
    });
  });
});
